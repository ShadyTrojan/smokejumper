﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaterBlast : MonoBehaviour {

    public ForceMode mode;
    public List<GameObject> deathObject;
    public Vector3 force;
    

    void ApplyForce()
    {
        rigidbody.AddForce(force, mode);
    }

    void OnCollisionEnter(Collision other)
    {


        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Water") 
        {
            Physics.IgnoreCollision(collider, other.collider);
        }
        else if(other.gameObject.tag == "Grenade")
        {
            //do nothing
        }
        else
        {
            Destroy(gameObject);
            if (deathObject != null)
            {
                foreach (GameObject death in deathObject)
                {
                    Instantiate(death, transform.position, Quaternion.identity);

                }
            }
        }
        
    }
    
}
