﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class playerMovement : MonoBehaviour 
{
    public Vector3 jumpSpeedPrimary;
    public Vector3 jumpSpeedSecondary;
    public ForceMode force;

    private int jumpCount;
    public int additionalJumpCount;
    private bool grounded;
    public string groundName;
    private float translation;
    public int fireTimer;
    private int fireCounter;
    private int fireSelector;
    public Vector3 fireVelocity;
    private Vector3 cameraLocation;
    public Camera camera;
    public List<GameObject> waterSplash;
    public Transform waterSpawnPoint;
    private GameObject water;
    public GameObject BounceEffect;
    public Transform bounceSpawnPoint;
    public GameObject Animator;  
    private bool inDownBlast;
    private float downBlastTime;
    public bool shootEnabled;

    public float groundAngle = 0.8f;
    protected bool paused;

    //****************
    //Shield
    //****************
    public GameObject shield;
    public float shieldLifeTime;
    private float shieldTimer;
    private bool playerShielded;
    public bool generateShield;

	// Use this for initialization
	void Start () 
    {
        jumpCount = 0;
        fireSelector = 0;
        inDownBlast = false;
        grounded = true;
        fireCounter = fireTimer;
        if (camera != null)
        {
            cameraLocation = transform.position - camera.transform.position;
            cameraLocation.y = 0;
        }
        shield.SetActive(false);
	}

	
	// Update is called once per frame
	void Update () 
    {
        if (!paused)
        {
            if (shootEnabled)
            {
                if (!inDownBlast)
                {

                    if (fireCounter <= 0)
                    {
                        fireSelector++;
                        if (fireSelector > waterSplash.Count - 1)
                            fireSelector = 0;

                        water = (GameObject)Instantiate(waterSplash[fireSelector], waterSpawnPoint.position, waterSpawnPoint.rotation);
                        water.SendMessage("ApplyForce");
                        fireCounter = fireTimer;
                    }

                }
                fireCounter--;
            }

            if (inDownBlast)
            {
                downBlastTime -= Time.deltaTime;
                if (downBlastTime <= 0)
                {
                    inDownBlast = false;
                }
            }



            if (Input.GetButtonDown("Jump"))
            {
                if (grounded || jumpCount <= additionalJumpCount)
                {
                    grounded = false;
                    if (jumpCount == 0)
                    {
                        Animator.GetComponent<tk2dSpriteAnimator>().Play("Jump");
                        rigidbody.AddForce(jumpSpeedPrimary, force);
                        //rigidbody.velocity = jumpSpeedPrimary;
                    }
                    else
                    {
                        Animator.GetComponent<tk2dSpriteAnimator>().Play("DownBlast");
                        downBlastTime = .5f;
                        inDownBlast = true;
                        rigidbody.AddForce(jumpSpeedSecondary, force);
                        //rigidbody.velocity = jumpSpeedSecondary;
                        if (BounceEffect != null)
                        {
                            Instantiate(BounceEffect, bounceSpawnPoint.position, bounceSpawnPoint.rotation);
                        }
                    }
                    jumpCount++;
                }
            }

            if (playerShielded)
            {
                shieldTimer -= Time.deltaTime;
                if (shieldTimer <= 0)
                {
                    playerShielded = false;
                    shield.SetActive(false);
                }

            }
            if (generateShield)
            {
                shield.SetActive(true);
                generateShield = false;
                playerShielded = true;
                shieldTimer = shieldLifeTime;
            }

        }
        
	}

    void OnPauseGame() { paused = true; }
    void OnResumeGame() { paused = false; }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "Grenade")
        {
            ContactPoint contact = collision.contacts[0];
            if (contact.normal.y >= groundAngle)
            {
                Animator.GetComponent<tk2dSpriteAnimator>().Play("Run");
                grounded = true;
                jumpCount = 0;
            }
        }
    }
}
