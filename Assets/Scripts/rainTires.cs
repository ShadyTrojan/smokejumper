﻿using UnityEngine;
using System.Collections;

public class rainTires : MonoBehaviour 
{
    public float delay;
    public float duration;
    private float totalDuration;
    public float sleepTime;
    public GameObject Tire;
    public float leftDist;
    public float rightDist;
    public Vector3 spawnVel;
    private float delayTime;
    public int explosionDelay;
    void Start()
    {
        delayTime = delay;
        totalDuration = 0;
    }
	void Update () 
    {
        totalDuration -= Time.deltaTime;
        if(totalDuration < -sleepTime)
        {
            totalDuration = duration;
        }
        if (totalDuration > 0)
        {
            delayTime -= Time.deltaTime;
            if (delayTime < 0)
            {
                delayTime += delay;
                Vector3 spawnPos = transform.position;
                spawnPos.x += Random.Range(-leftDist, rightDist);
                GameObject tempTire = (GameObject)Instantiate(Tire, spawnPos, Quaternion.identity);
                tempTire.transform.FindChild("Tire").rigidbody.velocity = spawnVel;
                tempTire.transform.FindChild("Fire").FindChild("FlamingTireLogic").GetComponent<FlamingTireLogic>().countDownTimer = explosionDelay;
            }
        }
	}
}
