﻿using UnityEngine;
using System.Collections;

public class TireBlastLogic : MonoBehaviour {
    public float lifeTimer;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        lifeTimer -= Time.deltaTime;
        if (lifeTimer <= 0)
        {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Hose" || other.gameObject.tag == "Ground" || other.gameObject.tag == "Water" || other.gameObject.tag == "FireBall")
        {
            //do nothing
            return;
        }

        if (other.gameObject.tag == "Player")
        {
            ForestLevelMechanics.tireMultiplier = 1;
            PlayerLife.ApplyDamage(40);          
        }
        else
        {
            other.gameObject.AddComponent<Decaytimer>().decayTime = .5f;
            other.gameObject.rigidbody.AddExplosionForce(5000, gameObject.transform.position, 1.5f);
        }

    }
}
