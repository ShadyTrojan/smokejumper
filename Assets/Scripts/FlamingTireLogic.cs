﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlamingTireLogic : MonoBehaviour {

    public GameObject tire;
    public int countDownTimer;
    private float deltaTimer;
    private tk2dTextMesh text;
    private BlowUpTire blow;

	// Use this for initialization
	void Start () {
        text = gameObject.GetComponent(typeof(tk2dTextMesh)) as tk2dTextMesh;
        blow = tire.GetComponent(typeof(BlowUpTire)) as BlowUpTire;
        
	}
	
	// Update is called once per frame
	void Update () {


        if (countDownTimer <= 0)
            blow.BlowTire();

        deltaTimer += Time.deltaTime;
        if(deltaTimer >= 1)
        {
            deltaTimer = 0;
            countDownTimer--;
        }

        text.text = countDownTimer.ToString();
	}
}
