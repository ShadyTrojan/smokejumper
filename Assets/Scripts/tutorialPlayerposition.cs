﻿using UnityEngine;
using System.Collections;

public class tutorialPlayerposition : MonoBehaviour {

    public GameObject position;
    private Vector3 myPos;
    private float x;

	// Use this for initialization
	void Start () {
        transform.position = position.transform.position;
        x = position.transform.position.x;
	}
    
    void OnEnable()
    {
        transform.position = position.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        myPos = transform.position;
        myPos.x = x;
        transform.position = myPos;
        //transform.position.Set(x, transform.position.y, transform.position.z);
        //if (transform.position.x != position.transform.position.x)
        //    transform.position.Set(0, transform.position.y, transform.position.z);
	}
}
