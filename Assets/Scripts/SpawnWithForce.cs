﻿using UnityEngine;
using System.Collections;

public class SpawnWithForce : MonoBehaviour 
{
    public ForceMode2D mode;
    //or use this as a spawning force without having to apply anything.
    public Vector3 force;
    public float rotMin;
    public float rotMax;
    public float levelSpeed;
    void Start()
    {
        //rigidbody2D.AddForce(force, mode);
        rigidbody.AddTorque(0,0,Random.Range(rotMin,rotMax));
    }
    void Update()
    {
        //transform.Translate(levelSpeed * Time.deltaTime, 0, 0);
    }
    //apply a force via gameObject.SendMessage("ApplyForce",Vector2 force)
    void ApplyForce(Vector2 force)
    {
        //rigidbody2D.AddForce(force, mode);
    }
    //apply a rotation via gameObject.SendMessage("ApplyRotation",float rot)
    void ApplyRotation(float rot)
    {
        rigidbody.AddTorque(0,0,rot);
    }
}
