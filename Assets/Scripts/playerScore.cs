﻿using UnityEngine;
using System.Collections;

public class playerScore : MonoBehaviour 
{
    public static int currScore=0;
    public float multiplierDecayRate;       //how long before you lose a score multiplier
    public float maxDecayTime;
    public static float multiplierTime;     //Current time left on the multiplier
    private int currentMultiplier;          //Store the current multiplier
    private int oldScore;
    private tk2dTextMesh textMesh;



	void Start () 
    {
        textMesh = GetComponent(typeof(tk2dTextMesh)) as tk2dTextMesh;
        currScore = 0;
        oldScore = currScore;
	}

    void Update()
    {
        currentMultiplier = ForestLevelMechanics.tireMultiplier;

        if (oldScore != currScore)
        {
            
            if(currentMultiplier > 1)
            {
                currScore = oldScore + ((currScore - oldScore) * currentMultiplier); //modify current incoming score to reflect the multiplier
            }

            textMesh.text = currScore.ToString();
            oldScore = currScore;
        }

        

        //trigger to add more time to the decayTime when you get another multiplier
        if(ForestLevelMechanics.addedMultiplier)
        {
            multiplierText.timeIsAlmostUp = false;
            ForestLevelMechanics.addedMultiplier = false;            
            multiplierTime += multiplierDecayRate;
            if (multiplierTime > maxDecayTime)
                multiplierTime = maxDecayTime;

        }
        if(multiplierTime <= 5)
            multiplierText.timeIsAlmostUp = true;
        

        //once the multiplier timer reaches 0, drop the multiplier back to 1
        if(currentMultiplier > 1)
        {
            multiplierTime -= Time.deltaTime;
            if(multiplierTime < 1)
            {
                multiplierText.timeIsAlmostUp = true;
            }

            if(multiplierTime <= 0)
            {
                ForestLevelMechanics.tireMultiplier = 1;
                multiplierText.timeIsAlmostUp = false;
            }
        }
    }
}
