﻿using UnityEngine;
using System.Collections;

public class enemyLife : MonoBehaviour 
{
    public float life;
    public GameObject deathObject;
    void ApplyDamage(float damage)
    {
        life -= damage;
        if (life <=0)
        {
            Destroy(gameObject);
            if(deathObject!=null)
            {
                Instantiate(deathObject, transform.position, transform.rotation);
            }
        }
    }

}
