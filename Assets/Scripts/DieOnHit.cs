﻿using UnityEngine;
using System.Collections;

public class DieOnHit : MonoBehaviour 
{
   

    void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Ground") 
        {
            Destroy(gameObject);                  
        }
    }
}
