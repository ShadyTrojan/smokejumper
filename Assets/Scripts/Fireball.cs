﻿using UnityEngine;
using System.Collections;

public class Fireball : MonoBehaviour 
{
    public GameObject Fire;
    public float spawnDelay;
    public float minX;
    public Vector3 vel;
    private float timer;
	void Update () 
    {
        transform.position += vel*Time.deltaTime;
	    if(transform.position.x < minX)
        {
            Destroy(this.gameObject);
        }
        timer -= Time.deltaTime;
        if(timer <=0)
        {
            timer += spawnDelay;
            Instantiate(Fire, transform.position, Quaternion.identity);
        }
	}
}

