﻿using UnityEngine;
using System.Collections;

public class ScrollBackground : MonoBehaviour {

    public GameObject levelLogic;
    public Vector3 startlocation;
    public float speed;
    public float respawnPoint;
    private float movement;
	// Use this for initialization
	void Start () {
        startlocation = transform.position;
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        movement = speed;
        movement *= Time.deltaTime;
        transform.Translate(movement, 0, 0);
        if(transform.position.x <= -20) //-12.81 original
        {
            transform.Translate(respawnPoint, 0, 0);
            if(tag == "Ground")
            {
                ForestLevelMechanics.levelDistance++; // update current "distance" into the level
            }
        }
	}
}
