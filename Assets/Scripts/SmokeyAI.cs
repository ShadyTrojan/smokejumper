﻿using UnityEngine;
using System.Collections;

public class SmokeyAI : MonoBehaviour 
{
    //alright:
    //ai phases, wait, spawn fireball, rain tires
    //on screen during spawn fireball/wait, offscreen + invuln during rain tires
    //ratios for each to happen, and 'middle ground' state
    //middle ground: wait, durations varied, wait duration? varied.
    public Transform idlePosition;      //where smokey sits normally
    public Transform exitPosition;      //where we consider smokey to have 'left'
    public Transform introPosition;
    public float maxHP = 1000;
    public GameObject healthbar;
    private Vector2 clip;

    public GameObject smokeyAnimator;
    private SmokeyAnimator animator;

    public GameObject Fireball;
    public int fireballAttackChance = 30;
    public float fireballDuration = 3;
    public Transform fireballSpawnPoint;
    public float fireballThrowDelay = 1.5f;
    public Vector3 fireballVel;
    public float fireballLifeTime = 3;

    public GameObject Tire;
    public int tireAttackChance = 10;
    public float tireDuration = 8;
    public Transform tireSpawnPoint;
    public float tireThrowDelay = 0.8f;
    public Vector3 tireVel;
    public int tireLifeTime = 2;
    public float tireLeftOffset = 2;    //this is the min x offset, it's randomized between -left and +right
    public float tireRightOffset = 2;   //this is the max x offset, it's randomized between -left and +right

    public float waitDuration = 2;
    public float retreatDuration = 0.3f;
    public float enterDuration = 0.5f;
    public float introDuration = 2.0f;

    private float health;
    private float hpCalc;
    private Rect healthRect;
    private bool vulnerable;
    private float timer;        //using this to keep track of state duration changes\
    private float attackTimer;
    public enum AIState
    {
        Waiting,
        Fireball,
        Tire,
        Entering,
        Exiting,
        Intro,
        Pause
    }
    public AIState currState;
	void Start () 
    {
        health = maxHP;
        healthRect = new Rect(0, 0, 1, 1);
        currState = AIState.Intro;
        timer += introDuration;
        transform.position = introPosition.position;
        animator = smokeyAnimator.GetComponent(typeof(SmokeyAnimator)) as SmokeyAnimator;
        clip = new Vector2(health / maxHP, 1);
	}
	void Update () 
    {
        timer -= Time.deltaTime;
        if (timer > 0)
        {
            //perform our states
            if (currState == AIState.Waiting)
            {
                Wait();
            }
            else if (currState == AIState.Fireball)
            {
                SpawnFireBalls();
            }
            else if (currState == AIState.Tire)
            {
                ThrowTires();
            }
            else if(currState == AIState.Entering)
            {
                Enter();
            }
            else if(currState == AIState.Exiting)
            {
                Exit();
            }
            else if(currState == AIState.Intro)
            {
                Intro();
            }
            else if(currState == AIState.Pause)
            {
                Pause();
            }
        }
        else
        {
            //these are all toggling states, the if's are just checking prior state
            if (currState == AIState.Waiting)
            {
                //choose an attack
                if (Random.Range(0, (float)tireAttackChance + fireballAttackChance) < (float)fireballAttackChance)
                {
                    //if we didn't choose fireball aka throw tires
                    animator.SetYellAnimation();
                    currState = AIState.Fireball;
                    timer += fireballDuration;
                }
                else
                {
                    animator.SetYellAnimation();
                    currState = AIState.Exiting;
                    vulnerable = false;
                    timer += retreatDuration;
                }
            }
            else if(currState == AIState.Exiting)
            {
                currState = AIState.Tire;
                timer += tireDuration;
            }
            else if(currState == AIState.Tire)
            {
                currState = AIState.Entering;
                animator.SetIdleAnimation();
                timer += enterDuration;
            }
            else if(currState == AIState.Entering)
            {
                currState = AIState.Waiting;
                vulnerable = true;
                timer += waitDuration;
            }
            else if(currState == AIState.Intro)
            {
                currState = AIState.Pause;
                timer += waitDuration;

            }
            else if(currState == AIState.Pause)
            {
                currState = AIState.Entering;
                timer += enterDuration;
            }
            else if(currState == AIState.Fireball)
            {
                animator.SetIdleAnimation();
                currState = AIState.Waiting;
                timer += waitDuration;
            }
        }
	}
    void SpawnFireBalls()
    {
        //spawnfireball
        attackTimer -= Time.deltaTime;
        if(attackTimer <= 0)
        {
            attackTimer += fireballThrowDelay;
            GameObject fireball = (GameObject)Instantiate(Fireball, fireballSpawnPoint.position, Quaternion.identity);
            fireball.rigidbody.velocity = fireballVel;
        }
    }
    void Wait()
    {
        //do waiting animation
    }
    void Pause()
    {
        //hang out off screen for a few seconds
    }
    void ThrowTires()
    {
        //throw tires
        attackTimer -= Time.deltaTime;
        if(attackTimer <= 0)
        {
            attackTimer += tireThrowDelay;
            Vector3 spawnPos = tireSpawnPoint.position;
            spawnPos.x += Random.Range(-tireLeftOffset, tireRightOffset);
            GameObject tire = (GameObject)Instantiate(Tire, spawnPos, Quaternion.identity);
            tire.transform.FindChild("Tire").rigidbody.velocity = tireVel;
            tire.transform.FindChild("Fire").FindChild("FlamingTireLogic").GetComponent<FlamingTireLogic>().countDownTimer = tireLifeTime;
        }
    }

    void Intro()
    {
        //Smokey's intro
        float time = (introDuration - timer) / introDuration;
        transform.position = Vector3.Lerp(introPosition.position, exitPosition.position, time);
        vulnerable = false;
    }
    void Enter()
    {
        //enter, then go to your point and sit
        float time = (enterDuration - timer) / enterDuration;
        transform.position = Vector3.Lerp(exitPosition.position, idlePosition.position, time);
    }
    void Exit()
    {
        //leave, 'vanish', or just sit invulnerable.
        float time = (enterDuration - timer) / enterDuration;
        transform.position = Vector3.Lerp(idlePosition.position, exitPosition.position, time);
    }
    public void ApplyDamage(float damage)
    {
        if(vulnerable)
        {
            health -= damage;
        }
        if(health <=0)
        {
            //die
            //perform death logic, call game screen/display score?
            Destroy(this.gameObject);
            Application.LoadLevel("WinScreen");
        }
        UpdateHealthBar();
    }
    void UpdateHealthBar()
    {
        
        hpCalc = health / maxHP;
        healthRect.width = hpCalc;
        healthbar.GetComponent<tk2dClippedSprite>().ClipRect = healthRect;
        //healthbar.GetComponent<tk2dClippedSprite>()._clipTopRight = clip; //adjust health bar
    }
}
