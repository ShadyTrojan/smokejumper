﻿using UnityEngine;
using System.Collections;

public class DeathMenuControl : tk2dUIBaseDemoController
{
    public bool disableOldWindows;
    public tk2dUIItem MainMenuButton;
    public tk2dUIItem RestartButton;
    public tk2dUIItem QuitButton;
    public tk2dUIItem[] ReturnToMenuButtons;
    public string firstLevel;
    public string mainMenu;


    private GameObject currWindow;

    void Awake()
    {
        //currWindow = MainMenuWindow;
        //ShowWindow(MainMenuWindow.transform);
    }

    void OnEnable()
    {
        MainMenuButton.OnClick += GoToMainMenu;
        RestartButton.OnClick += GoToGame;
        QuitButton.OnClick += ExitGame;
    }

    private void GoToMainMenu()
    {
        Application.LoadLevel(mainMenu);
    }

    private void GoToGame()
    {
        Application.LoadLevel(firstLevel);
    }
    private void ExitGame()
    {
        Application.Quit();
    }
}
