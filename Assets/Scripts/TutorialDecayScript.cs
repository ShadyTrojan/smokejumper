﻿using UnityEngine;
using System.Collections;

public class TutorialDecayScript : MonoBehaviour {

    public float decayTime;
    private float time;

	// Use this for initialization
	void Start () {
        time = decayTime;
	}
	
	// Update is called once per frame
	void Update () {

	    time -= Time.deltaTime;
        if (time <= 0)
        {
            GameObject.Find("Player Character").GetComponent<playerMovement>().generateShield = true;
            Destroy(gameObject);
        }
	}
}
