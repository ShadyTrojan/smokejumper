﻿using UnityEngine;
using System.Collections;

public class countMolys : MonoBehaviour {

    private tk2dTextMesh text;

	// Use this for initialization
	void Start () {
        text = GetComponent(typeof(tk2dTextMesh)) as tk2dTextMesh;
	}
	
	// Update is called once per frame
	void Update () {
        text.text = GameObject.FindGameObjectsWithTag("Grenade").Length.ToString();
	}
}
