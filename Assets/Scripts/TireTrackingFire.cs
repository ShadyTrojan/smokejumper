﻿using UnityEngine;
using System.Collections;

public class TireTrackingFire : MonoBehaviour {

    public GameObject tire;
    public Vector3 tirePos;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
       
        tirePos = tire.transform.position;
        tirePos.x -= .05f;
        tirePos.y += .21f;
        
        transform.position = tirePos;
        

	}
}
