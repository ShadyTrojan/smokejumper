﻿using UnityEngine;
using System.Collections;

public class Decaytimer : MonoBehaviour 
{

    public float decayTime;
    private float time;
    private BlowUpTire blow;
    private Life life;
	
    void Start()
    {
        time = decayTime;
    }
	void Update () 
    {
        time -= Time.deltaTime;
        if(time <=0)
        {
            if(gameObject.tag == "Tire")
            {
                //ForestLevelMechanics.tireMultiplier++;
                ForestLevelMechanics.addedMultiplier = true;
                blow = gameObject.GetComponent(typeof(BlowUpTire)) as BlowUpTire;
                blow.BlowTire();
                return;
            }
            else if(gameObject.tag == "Grenade")
            {
                life = gameObject.GetComponent(typeof(Life)) as Life;
                life.ApplyDamage(100, true);
            }
            else if(gameObject.tag == "Water")
            {
                GameObject.Find("Player Character").GetComponent<playerMovement>().generateShield = true;
                Destroy(gameObject);
            }
            else
                Destroy(gameObject);
        }
	}
}
