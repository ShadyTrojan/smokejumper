﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour 
{
	public GameObject pauseScreen;
	
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.P)) 
		{
			pauseScreen.SetActive (true);
		}
	}
}
