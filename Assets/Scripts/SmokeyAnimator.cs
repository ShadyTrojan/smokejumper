﻿using UnityEngine;
using System.Collections;

public class SmokeyAnimator : MonoBehaviour {

    private tk2dSpriteAnimator animator;

    void Start()
    {
        animator = gameObject.GetComponent(typeof(tk2dSpriteAnimator)) as tk2dSpriteAnimator;
    }
	
    //set animator to "x" animation
    public void SetIdleAnimation()
    {
        animator.Play("Smoking");
    }
    public void SetYellAnimation()
    {

        animator.Play("TireYell");
    }

}
