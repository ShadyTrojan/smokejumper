﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlaynomicsPlugin;

public class SpawnMolys : MonoBehaviour {

    public static Queue<GameObject> queue;
    //private ConcurrentQueue<GameObject> queue;
    private GameObject bob;
    private GameObject bob2;
    private int difference;


    private int max;
    
	
	void Start() {
        queue = new Queue<GameObject>(10);
        max = ForestLevelMechanics.maxMolitovsAllowed;
        //System.GC.KeepAlive(queue);
        //queue = new ConcurrentQueue<GameObject>();
	}
	
	
	void Update () 
    {
        
        for (int i = 0; i < queue.Count; i++)
        {
            //if (queue.Count == 0)
            //    break;
            if (ForestLevelMechanics.numActiveMolitovs == max)
            {
                
                break;
            }
                
            bob = queue.Dequeue();
            bob.SetActive(true);

        }

        for (int i = 0; i < queue.Count; i++)
        {
            Destroy(queue.Dequeue());
        }


        //if (queue.Count != 0)
        //{
        //    difference = ForestLevelMechanics.maxMolitovsAllowed - ForestLevelMechanics.numActiveMolitovs;
        //    if (difference > 0)
        //    {
        //        for (int i = 0; i < difference; i++)
        //        {
        //            if (queue.Count == 0)
        //                break;
        //            bob = queue.Dequeue();
        //            bob.SetActive(true);

        //        }

        //    }

        //    for (int i = 0; i < queue.Count; i++)
        //    {
        //        Destroy(queue.Dequeue());
        //    }
        //}
	}

    //public void AddMoly(GameObject go)
    //{
    //    Debug.Log(go);
    //    queue.Enqueue(bob2);
    //    queue.ToString();

    //    queue.Enqueue(go);
    //}
}
