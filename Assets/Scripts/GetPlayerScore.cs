﻿using UnityEngine;
using System.Collections;

public class GetPlayerScore : MonoBehaviour
{

    private tk2dTextMesh text;

    // Use this for initialization
    void Start()
    {
        text = GetComponent(typeof(tk2dTextMesh)) as tk2dTextMesh;
        int score = playerScore.currScore;
        text.text = score.ToString();

    }

}