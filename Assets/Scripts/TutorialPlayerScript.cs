﻿using UnityEngine;
using System.Collections;

public class TutorialPlayerScript : MonoBehaviour {

    public Vector3 jumpSpeedPrimary;
    public Vector3 jumpSpeedSecondary;
    public ForceMode force;

    private int jumpCount;
    public int additionalJumpCount;
    private bool grounded;
    public string groundName;
   
    
    public Transform waterSpawnPoint;
    private GameObject water;
    public GameObject BounceEffect;
    public Transform bounceSpawnPoint;
    public GameObject Animator;
    private bool inDownBlast;
    private float downBlastTime;
    public bool shootEnabled;

    public float groundAngle = 0.8f;
    protected bool paused;

    //****************
    //Shield
    //****************
    public GameObject shield;
    public float shieldLifeTime;
    private float shieldTimer;
    private bool playerShielded;
    public bool generateShield;

    //jump timer
    private bool firstJump;
    private bool secondJump;
    public float jumpTimer;
    public float doubleJumpTimer;
    private float jumpTime;
    private bool readyToDoubleJump;
    public bool readyToJump;

    // Use this for initialization
    void Start()
    {
        jumpCount = 0;
        firstJump = true;
        jumpTime = jumpTimer;
        inDownBlast = false;
        grounded = true;
        readyToJump = true;
        shield.SetActive(false);
    }

    void OnEnable()
    {
        jumpCount = 0;
        firstJump = true;
        inDownBlast = false;
        grounded = true;
        jumpTime = jumpTimer;
        shield.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        if (!paused)
        {
            if (inDownBlast)
            {
                downBlastTime -= Time.deltaTime;
                if (downBlastTime <= 0)
                {
                    inDownBlast = false;
                }
            }
            if (firstJump)
            {
                jumpTime -= Time.deltaTime;
                if (jumpTime <= 0)
                {
                    readyToJump = true;
                }
            }
            if(secondJump)
            {
                jumpTime -= Time.deltaTime;
                if(jumpTime <= 0)
                {
                    readyToJump = true;
                }
            }

            Debug.Log(jumpTime);

            if (readyToJump)
            {
                if(firstJump)
                {
                    readyToJump = false;
                    firstJump = false;
                    secondJump = true;
                    jumpTime += jumpTimer;
                }
                if(secondJump)
                {
                    firstJump = true;
                    secondJump = false;
                    readyToJump = false;
                    jumpTime += 1;
                }
                
                
                if (grounded || jumpCount <= additionalJumpCount)
                {
                    grounded = false;
                    if (jumpCount == 0)
                    {
                        Animator.GetComponent<tk2dSpriteAnimator>().Play("Jump");
                        rigidbody.AddForce(jumpSpeedPrimary, force);
                        //rigidbody.velocity = jumpSpeedPrimary;
                    }
                    else
                    {
                        
                        Animator.GetComponent<tk2dSpriteAnimator>().Play("DownBlast");
                        downBlastTime = .5f;
                        inDownBlast = true;
                        rigidbody.AddForce(jumpSpeedSecondary, force);
                        //rigidbody.velocity = jumpSpeedSecondary;
                        if (BounceEffect != null)
                        {
                            Instantiate(BounceEffect, bounceSpawnPoint.position, bounceSpawnPoint.rotation);
                        }
                    }
                    jumpCount++;
                }
                readyToDoubleJump = true;
            }

            if (playerShielded)
            {
                shieldTimer -= Time.deltaTime;
                if (shieldTimer <= 0)
                {
                    playerShielded = false;
                    shield.SetActive(false);
                }

            }
            if (generateShield)
            {
                shield.SetActive(true);
                generateShield = false;
                playerShielded = true;
                shieldTimer = shieldLifeTime;
            }

        }

    }

    void OnPauseGame() { paused = true; }
    void OnResumeGame() { paused = false; }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "Grenade")
        {
            ContactPoint contact = collision.contacts[0];
            if (contact.normal.y >= groundAngle)
            {
                Animator.GetComponent<tk2dSpriteAnimator>().Play("Run");
                grounded = true;
                jumpCount = 0;
            }
        }
    }
}
