﻿using UnityEngine;
using System.Collections;

public class DamageEnemies : MonoBehaviour 
{
    public float damage;
    public int playerLayer = 9;
    public int[] desiredMoliSpawns;
    public Transform explosionPoint;
    private Life life;

    void Start()
    {
        if(gameObject.layer == 16)
        {
            explosionPoint = GameObject.Find("DownBlastForcePoint").transform;
        }
    }
    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Grenade")
    //    {
    //        life = other.gameObject.GetComponent(typeof(Life)) as Life;
    //        life.ApplyDamage(damage);
    //    }
    //}
    void OnCollisionEnter(Collision other)
    {
        if(this.gameObject.layer == 16)
        {
            if (other.gameObject.tag == "Fire")
            {
                //other.gameObject.rigidbody.AddExplosionForce(2500, explosionPoint.position, 5f);
                life = other.gameObject.GetComponent(typeof(Life)) as Life;
                life.ApplyDamageWithScore(damage);
            }
            if (other.gameObject.tag == "Grenade")
            {
                //other.gameObject.rigidbody.AddExplosionForce(2500, explosionPoint.position, 5f);
                life = other.gameObject.GetComponent(typeof(Life)) as Life;
                life.ApplyDamageWithScore(damage, desiredMoliSpawns);
            }
            if (other.gameObject.tag == "Tire")
            {
                //other.gameObject.rigidbody.AddExplosionForce(2500, explosionPoint.position, 5f, 3f);
            }
        }
        if(this.gameObject.layer == 14)
        {
            
            if (other.gameObject.tag == "Fire")
            {
                
                life = other.gameObject.GetComponent(typeof(Life)) as Life;
                life.ApplyDamageWithScore(damage);
            }
            if (other.gameObject.tag == "Grenade")
            {
                
                life = other.gameObject.GetComponent(typeof(Life)) as Life;
                life.ApplyDamageWithScore(damage, desiredMoliSpawns);
            }
        }
        if (other.gameObject.tag == "Fire")
        {
            life = other.gameObject.GetComponent(typeof(Life)) as Life;
            life.ApplyDamageWithScore(damage);
        }
        if (other.gameObject.tag == "Enemy" )
        {
            life = other.gameObject.GetComponent(typeof(Life)) as Life;
            life.ApplyDamageWithScore(damage);
        }
        if (other.gameObject.tag == "Grenade")
        {
            life = other.gameObject.GetComponent(typeof(Life)) as Life;
            life.ApplyDamageWithScore(damage, desiredMoliSpawns);
        }
        if(other.gameObject.tag == "Smokey")
        {
            SmokeyAI smokeyLife = other.gameObject.GetComponent(typeof(SmokeyAI)) as SmokeyAI;
            smokeyLife.ApplyDamage(damage);
        }
    }
    
}
