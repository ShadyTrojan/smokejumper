﻿using UnityEngine;
using System.Collections;

public class IgnorePlayerCollision : MonoBehaviour {

    public bool IgnorePlayer;
    public int playerLayer;
    public float damage;
    public float damageTimer;
    private float time;


	void Start () 
    {
        time = 0;
        if (IgnorePlayer)
        {
            Physics2D.IgnoreLayerCollision(gameObject.layer, playerLayer);
        }
	}
	
	// Update is called once per frame
	void Update () {

        time -= Time.deltaTime;
	
	}

    void OnCollisionEnter(Collision other)
    {

        if(other.gameObject.tag == "Hose" || other.gameObject.tag == "Enemy" || other.gameObject.tag == "Tire")
        {
            Physics.IgnoreCollision(collider, other.collider);
            return;
        }
        if (other.gameObject.layer == 14)
            return;

        if (other.gameObject.tag == "Player")
        {
            
            if (time <= 0)
            {

                PlayerLife.ApplyDamage(damage);
                time = damageTimer;
            }
            //other.collider.gameObject.SendMessage("ApplyDamage", damage);
            Physics.IgnoreCollision(collider, other.collider);
        }
        
    }
}
