﻿using UnityEngine;
using System.Collections;

public class GetPlayerMultiplier : MonoBehaviour {

    private tk2dTextMesh text;

    // Use this for initialization
    void Start()
    {
        text = GetComponent(typeof(tk2dTextMesh)) as tk2dTextMesh;
        int score = ForestLevelMechanics.highestMultiplier;
        text.text = "x" + score.ToString();

    }
}
