﻿using UnityEngine;
using System.Collections;

public class MolotovCollision : MonoBehaviour {

    private Life life;

	void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Player" || other.gameObject.tag == "Ground")
        {
            life = this.gameObject.GetComponent(typeof(Life)) as Life;
            life.ApplyDamage(100);
        }
        if(other.gameObject.tag == "Hose")
        {
            life = this.gameObject.GetComponent(typeof(Life)) as Life;
            life.ApplyDamage(100, true);
        }
    }
}
