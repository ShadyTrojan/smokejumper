﻿using UnityEngine;
using System.Collections;

public class TutorialHpRegen : MonoBehaviour {

    private float maxHP= 70;
    private float currentHP = 10;
    private float healing = 5;
    private float hpCalc;
    private Rect hpRect;
    private tk2dClippedSprite sprite;

	// Use this for initialization
	void Start () {
        currentHP = 10;
        hpRect = new Rect(0, 0, currentHP / maxHP, 1);
        sprite = GetComponent(typeof(tk2dClippedSprite)) as tk2dClippedSprite;
	}
    void OnEnable()
    {
        currentHP = 10;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (currentHP < maxHP)
        {
            currentHP += healing * Time.deltaTime;
        }

        hpCalc = currentHP / maxHP;
        hpRect.width = hpCalc;
        sprite.ClipRect = hpRect;
    }
}
