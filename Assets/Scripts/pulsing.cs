﻿using UnityEngine;
using System.Collections;

public class pulsing : MonoBehaviour 
{
    public float delay;
    private float delayTime;
    public float minSize;
    public float maxSize;
    public float maxChange;
	void Update () 
    {
        delayTime -= Time.deltaTime;
        if(delayTime<0)
        {
            delayTime += delay;
            //make a range
            float currScale = transform.localScale.x;
            float scale = Random.Range(currScale - maxChange, currScale + maxChange);
            if(scale < minSize)
            {
                scale = minSize;
            }
            else if(scale > maxSize)
            {
                scale = maxSize;
            }
            transform.localScale = new Vector3(scale, scale, scale);
        }
	}
}
