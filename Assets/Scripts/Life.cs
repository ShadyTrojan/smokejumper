﻿using UnityEngine;
using System.Collections;

public class Life : MonoBehaviour 
{
    public float life;
    public int score = 0;
    public GameObject[] deathObject;
    private GameObject scoreObject;
    //private Vector3 vel;
    void Start()
    {
       
    }
    void Update()
    {
        if(transform.position.y < -5f || transform.position.x < -7f)
        {
            Destroy(gameObject);
            if (tag == "Grenade")
            {
                ForestLevelMechanics.numActiveMolitovs--;
            }
        }
        
        
    }

    public void ApplyDamageWithScore(float damage)
    {
        life -= damage;
        if (life <= 0)
        {
            playerScore.currScore += score;
            Kill();
        }
    }

    public void ApplyDamageWithScore(float damage, int[] spawnlist)
    {
        life -= damage;
        if (life <= 0)
        {
            playerScore.currScore += score;

            Kill(spawnlist);
        }
    }

    public void ApplyDamageWithScore(float damage , bool noSpawns)
    {
        life -= damage;
        if (life <= 0)
        {
            playerScore.currScore += score;
            KillNoSpawns();
        }
    }

    /// <summary>
    /// Damage the target. If it ties as a result, Spawn all death Objects
    /// Used to kill an enemy from collision or other non player interactions
    /// </summary>
    /// <param name="damage"></param>
    public void ApplyDamage(float damage)
    {
        life -= damage;
        if (life <=0)
        {
            Kill();
        }
    }
    /// <summary>
    /// Damage the target. If it dies, spawn only the given list of items
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="spawnlist"></param>
    public void ApplyDamage(float damage, int[] spawnlist)
    {
        life -= damage;
        if (life <= 0)
        {
            Kill(spawnlist);
        }
    }
    /// <summary>
    /// Damage the target. If it dies, do not spawn anything
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="noSpawns"></param>
    public void ApplyDamage(float damage, bool noSpawns)
    {
        life -= damage;
        if (life <= 0)
        {
            KillNoSpawns();
        }
    }

    public void Kill()
    {
        if (tag == "Grenade")
        {
            ForestLevelMechanics.numActiveMolitovs--;
        }
        Destroy(gameObject);
        if (deathObject != null)
        {
            foreach (GameObject death in deathObject)
            {
                
                Instantiate(death, transform.position, Quaternion.identity);
            }
        }
    }

    public void Kill(int[] spawnList)
    {
        if (tag == "Grenade")
        {
            ForestLevelMechanics.numActiveMolitovs--;
        }
        Destroy(gameObject);
        if (deathObject != null)
        {
            for(int i = 0; i < spawnList.Length; i++)
            {
                Instantiate(deathObject[spawnList[i]], transform.position, Quaternion.identity);
            }
        }
    }
    
    public void KillNoSpawns()
    {
        if (tag == "Grenade")
            ForestLevelMechanics.numActiveMolitovs--;
        Destroy(gameObject);
    }
}
