﻿using UnityEngine;
using System.Collections;

public class TutorialTireSpawner : MonoBehaviour {

    public GameObject tutorialTire;
    public Transform tireSpawn;
    private GameObject tire;
    private bool TireIsAlive;
    private float spawnTimer = .5f;

	// Use this for initialization
	void Start () {
        //tire = (GameObject)Instantiate(tutorialTire, transform.position, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
        
        if(!tire)
        {
            spawnTimer -= Time.deltaTime;
        }

        if(spawnTimer <= 0)
        {
            tire = (GameObject)Instantiate(tutorialTire, transform.position, Quaternion.identity);
            tire.transform.parent = this.gameObject.transform;
            spawnTimer = 3;
        }
	    
	}
}
