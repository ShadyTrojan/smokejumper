﻿using UnityEngine;
using System.Collections;

public class multiplierText : MonoBehaviour {

    private tk2dTextMesh text;
    private MeshRenderer mesh;
    private Color c;
    private Color cTo;
    private Color cFrom;
    private Color final;
    private float t;
    private bool increasing;
    private bool decreasing;
    private bool adding;
    public static bool timeIsAlmostUp;
    public float lerpTime = 1;
    public Color lerpTo;
    private int multiplier;

	void Start () {
        text = gameObject.GetComponent(typeof(tk2dTextMesh)) as tk2dTextMesh;
        mesh = gameObject.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
        c = text.color;
        increasing = true;
        cFrom = c;
        cTo = lerpTo;
        timeIsAlmostUp = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (timeIsAlmostUp)
        {
            if (mesh.enabled == false)
                mesh.enabled = true;
            text.color = FastInterpolateColors();
        }
        else
        {
            if (mesh.enabled == false)
                mesh.enabled = true;
            text.color = NormalInterpolateColors();
        }


        multiplier = ForestLevelMechanics.tireMultiplier;
        if (multiplier > 1)
        {
            if (mesh.enabled == false)
                mesh.enabled = true;

        }
        else

            mesh.enabled = false;

         text.text = "x" + multiplier;
	}

    Color NormalInterpolateColors()
    {
        if(increasing)
        {
            adding = true;
            increasing = false;
        }
        if(decreasing)
        {
            adding = false;
            decreasing = false;
        }
        
        if(adding)
        {
            t += Time.deltaTime / lerpTime;
            if (t >= 1)
                decreasing = true;
            
            final = Color.Lerp(cFrom, cTo, t);
            final.a = 1f;
            return final;
        }
        else
        {
            t -= Time.deltaTime / lerpTime;
            if (t <= 0)
                increasing = true;
            
            final = Color.Lerp(cFrom, cTo, t);
            final.a = 1f;
            return final;
        }
    }

    Color FastInterpolateColors()
    {
        if (increasing)
        {            
            adding = true;
            increasing = false;
        }
        if (decreasing)
        {
            adding = false;
            decreasing = false;
        }

        if (adding)
        {
            t += Time.deltaTime / .2f;   //Faster
            if (t >= 1)
                decreasing = true;

            final = Color.Lerp(cFrom, cTo, t);
            final.a = 1f;
            return final;
        }
        else
        {
            t -= Time.deltaTime / .2f;   //faster
            if (t <= 0)
                increasing = true;

            final = Color.Lerp(cFrom, cTo, t);
            final.a = 1f;
            return final;
        }
    }
}
