﻿using UnityEngine;
using System.Collections;

public class PlayerLife : MonoBehaviour {
    public static float invulnTime = 0.05f;
    private static float invulnDelay;
    public float life = 70;                  //max set to 70 currently
    public float regenTime;
    public int healing;
    public GameObject deathObject;
    public GameObject healthbar;
    private Vector3 vel;
    private Vector2 clip;
    private float regenTimer;   
    private float maxLife;
    private static float recentDamage;
    private static bool tookDamageRecently;

	// Use this for initialization
	void Start () {
        //if (life != 70)
        //    life = 70;
        maxLife = life;
        clip = new Vector2(life / 100f, 1);
	}
	
	// Update is called once per frame
	void Update () {
        if (invulnDelay > 0)
        {
            invulnDelay -= Time.deltaTime;
        }

        if(tookDamageRecently)
        {
            life -= recentDamage;
            recentDamage = 0;
        }

        //regen life
        if(life < maxLife)
        {
            //check if player lost more life within certain amount of time
            if (tookDamageRecently)
            {
                regenTimer = regenTime;
                tookDamageRecently = false;
            }
            else
            {
                regenTimer -= Time.deltaTime;
                if (regenTimer <= 0)
                {
                    if(life>=maxLife)
                    {
                        life = maxLife;
                    }
                    else
                    {
                        life += healing * Time.deltaTime;
                    }
                }
            }

        }

        //update healthbar
        UpdateHealthBar();

        if (life <= 0)
        {
            KillPlayer();
        }

	}

    void UpdateHealthBar()
    {
        clip.x = life / 100f;
        healthbar.GetComponent<tk2dClippedSprite>()._clipTopRight = clip; //adjust health bar
    }

    void KillPlayer()
    {
        //vel = rigidbody.velocity;
        //Destroy(gameObject);
        if (deathObject != null)
        {
            Instantiate(deathObject, transform.position, transform.rotation);
            deathObject.rigidbody.AddForce(vel);
        }
        
        Application.LoadLevel(2); //DeathScreen
    }

    public static void ApplyDamage(float damage)
    {
        if (invulnDelay <= 0)
        {
            invulnDelay = invulnTime;
            tookDamageRecently = true;
            recentDamage += damage;
        }
    }
}
