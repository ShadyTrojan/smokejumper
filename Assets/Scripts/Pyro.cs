﻿using UnityEngine;
using System.Collections;

public class Pyro : MonoBehaviour 
{

    public GameObject molotov;
    private GameObject thrownMolotov;
    public float maxMolotovRotation;
    public Transform molotovSpawnPoint;
    public GameObject player;
    public float attackDelay;
    private float attkTimer;
    public float disengageRange;
    public float attackRange;
    public float moveSpeed;
    private float distanceToPlayer;
    private enum PyroAttackState { Attacking, HoldFire }
    private enum PyroMoveState { ClosingDistance, Retreating }
    private PyroMoveState movState;
    private PyroAttackState attkState;
    public float speed;
    private float movement;
    private float speedFallOff;
    private float disengageSpeed;
    private int activeMoly;
    private int maxMoly;

    

    //********************
    //Molitov Trajectory 
    public Vector2 timeToImpactRange;
    private float timeTillImpact;
    private Vector3 target;
    private Vector3 spawn;
    private Vector3 distance;
    private Vector3 velocity;

    public GameObject molyTracker;
    private SpawnMolys spawner;
    private int tripped;


    void Start()
    {
        //Physics.IgnoreLayerCollision(8, 8);
        Physics.IgnoreLayerCollision(8, 9);
        Physics.IgnoreLayerCollision(8, 10);
        Physics.IgnoreLayerCollision(8, 11);
        Physics.IgnoreLayerCollision(8, 12);
        Physics.IgnoreLayerCollision(8, 13);

        velocity = new Vector3();
        attackRange = Mathf.Abs(attackRange);
        maxMoly = ForestLevelMechanics.maxMolitovsAllowed;
        activeMoly = ForestLevelMechanics.numActiveMolitovs;
        spawner = molyTracker.GetComponent(typeof(SpawnMolys)) as SpawnMolys;
        
    }

    // Update is called once per frame
    void Update()
    {
        activeMoly = ForestLevelMechanics.numActiveMolitovs;
        if (transform.position.y < -5f)
            Destroy(gameObject);

        //Move with ground speed
        movement = speed * Time.deltaTime;
        distanceToPlayer = Vector2.Distance(player.transform.position, transform.position);

        CheckPyroStates();
        if (attkState == PyroAttackState.Attacking)
        {
            Attack();
        }
        Retreat();
        

        transform.Translate(movement, 0, 0);
    }
    void CheckPyroStates()
    {
        // decrement the time till pyro can attack
        if (attkTimer > 0)
        {
            attkTimer -= Time.deltaTime;
        }

        // if the player is out of range or it isn't time to attack or too many molitovs already then hold fire
        if (distanceToPlayer > attackRange || attkTimer > 0 || activeMoly >= maxMoly)
        {
            attkState = PyroAttackState.HoldFire;
        }
        else
        {
            attkState = PyroAttackState.Attacking;
        }

    }
    void Attack()
    {
        attkTimer += attackDelay; // Pyro has attacked, reset the timer
        if (ForestLevelMechanics.numActiveMolitovs < ForestLevelMechanics.maxMolitovsAllowed)
        {
            ForestLevelMechanics.numActiveMolitovs++;
            thrownMolotov = (GameObject)Instantiate(molotov, molotovSpawnPoint.position, molotovSpawnPoint.rotation);
            thrownMolotov.rigidbody.velocity = CalculateTrajectory();
            thrownMolotov.rigidbody.AddTorque(0, 0, Random.Range(.1f, maxMolotovRotation));
        }



        //attkTimer += attackDelay; // Pyro has attacked, reset the timer

        ////perform the attack
        ////currently uses a static timer to set the arc for the trajectory
        //activeMoly = ForestLevelMechanics.numActiveMolitovs;
        ////Debug.Log("active moly " + activeMoly);
        //if(activeMoly >= maxMoly)
        //{
        //    Debug.Log(++tripped);
        //    attkState = PyroAttackState.HoldFire;
        //    return;
        //}
        
        //thrownMolotov = (GameObject)Instantiate(molotov, molotovSpawnPoint.position, molotovSpawnPoint.rotation);
        //thrownMolotov.rigidbody.velocity = CalculateTrajectory();
        //thrownMolotov.rigidbody.AddTorque(0, 0, Random.Range(.1f, maxMolotovRotation));
        //thrownMolotov.SetActive(false);
        //SpawnMolys.queue.Enqueue(thrownMolotov);
        //spawner.AddMoly(thrownMolotov);


        //ForestLevelMechanics.numActiveMolitovs++; //update forestLevel so it knows how many active molitovs there are.
    }
    Vector3 CalculateTrajectory()
    {
        target = player.transform.position;
        spawn = molotovSpawnPoint.transform.position;

        distance = target - spawn;

        timeTillImpact = Random.Range(timeToImpactRange.x, timeToImpactRange.y);

        velocity.y = -Physics.gravity.y * (timeTillImpact / 2);
        velocity.x = (distance.x + .5f) / timeTillImpact;

        return velocity;
    }
    void Retreat()
    {
        if (movState == PyroMoveState.Retreating)
        {
            speedFallOff = disengageSpeed * .002f;
            disengageSpeed -= speedFallOff;
            movement += disengageSpeed * Time.deltaTime;
            if (disengageSpeed <= moveSpeed)
            {
                movState = PyroMoveState.ClosingDistance;

            }
            return;
        }
        movement = speed * Time.deltaTime;
        if (distanceToPlayer <= disengageRange)
        {
            disengageSpeed = moveSpeed * 1.9f;
            movState = PyroMoveState.Retreating;
            movement += disengageSpeed * Time.deltaTime;
        }
    }
    void OnCollisionEnter(Collision other)
    {
        //if(other.gameObject.tag == "Player" || other.gameObject.tag == "Grenade" 
        //    || other.gameObject.tag == "Attack" || other.gameObject.tag == "Tire"
        //    || other.gameObject.tag == "Enemy")
        //{
        //    Physics.IgnoreCollision(collider, other.collider);
        //}
    }
}
