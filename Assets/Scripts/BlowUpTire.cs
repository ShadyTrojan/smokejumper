﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlowUpTire : MonoBehaviour {


    public GameObject killObject;
    public int tireScore;
    public List<GameObject> deathObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void BlowTire()
    {
        playerScore.currScore += tireScore;
        ForestLevelMechanics.tireMultiplier++;
        Destroy(killObject);

        if (deathObject != null)
        {
            foreach (GameObject death in deathObject)
            {
                Instantiate(death, transform.position, Quaternion.identity);

            }
        }
    }
}
