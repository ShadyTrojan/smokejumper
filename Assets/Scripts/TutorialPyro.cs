﻿using UnityEngine;
using System.Collections;

public class TutorialPyro : MonoBehaviour {

    public GameObject molotov;
    private GameObject thrownMolotov;
    public float maxMolotovRotation;
    public Transform molotovSpawnPoint;
    public GameObject player;
    public float attackDelay;
    private float attkTimer;
    private float distanceToPlayer;



    //********************
    //Molitov Trajectory 
    public Vector2 timeToImpactRange;
    private float timeTillImpact;
    private Vector3 target;
    private Vector3 spawn;
    private Vector3 distance;
    private Vector3 velocity;

    //public GameObject molyTracker;


    void Start()
    {
        attkTimer = attackDelay;
        velocity = new Vector3();


    }

    // Update is called once per frame
    void Update()
    {


        distanceToPlayer = Vector2.Distance(player.transform.position, transform.position);



        if (attkTimer <= 0)
        {
            
            Attack();
        }
        else
        {
            attkTimer -= Time.deltaTime;
        }

    }
    void Attack()
    {
        attkTimer += attackDelay; // Pyro has attacked, reset the timer
        
        thrownMolotov = (GameObject)Instantiate(molotov, molotovSpawnPoint.position, molotovSpawnPoint.rotation);
        thrownMolotov.rigidbody.velocity = CalculateTrajectory();
        thrownMolotov.rigidbody.AddTorque(0, 0, Random.Range(.1f, maxMolotovRotation));
        
    }
    Vector3 CalculateTrajectory()
    {
        target = player.transform.position;
        spawn = molotovSpawnPoint.transform.position;

        distance = target - spawn;

        timeTillImpact = Random.Range(timeToImpactRange.x, timeToImpactRange.y);

        velocity.y = -Physics.gravity.y * (timeTillImpact / 2);
        velocity.x = (distance.x + .5f) / timeTillImpact;

        return velocity;
    }


}
