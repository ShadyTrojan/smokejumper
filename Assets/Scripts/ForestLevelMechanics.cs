﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ForestLevelMechanics : MonoBehaviour {

    public List<GameObject> Enemies;
    public GameObject smokey;
    public GameObject smokeysHealth;
    public Vector3 enemySpawnPosition;          // (8.0, 0,0) is just offscreen
    public Vector2 spawnTimerRange;             // min and max time for enemy spawns
    public int maxMoly;
    public int scoreToSpawnBoss = 10000;
    public static int numActiveMolitovs;        // Limit spawns
    public static int maxMolitovsAllowed = 5;       // Limit Spawns
    public static int levelDistance;
    public static int tireMultiplier = 1;
    public static int highestMultiplier;
    public static bool addedMultiplier;
    private float spawnTimer;                   // store the count down timer
    private int currentDistance;                //holds the current groundCounter to check against
    private float spawnChecker;                 //Store a modded result here to see if it's time to spawn something
    private bool enemyJustSpawned;
    private int enemySpawnCounter;              //Store spawn counter to move spawn position for each enemy
    private bool readyToSpawnBoss;


	void Start () {
        readyToSpawnBoss = true;
        smokey.SetActive(false);
        smokeysHealth.SetActive(false);
        currentDistance = levelDistance;
        spawnTimer = Random.Range(spawnTimerRange.x, spawnTimerRange.y);
        tireMultiplier = 1;
        highestMultiplier = tireMultiplier;
        //maxMolitovsAllowed = maxMoly;
	}
	
	// Update is called once per frame
	void Update () 
    {

        if (highestMultiplier < tireMultiplier)
            highestMultiplier = tireMultiplier;

        if (readyToSpawnBoss)
        {
            if (playerScore.currScore > scoreToSpawnBoss)
            {
                smokey.SetActive(true);
                smokeysHealth.SetActive(true);
                readyToSpawnBoss = false;
                spawnTimerRange.x = 2f;
                spawnTimerRange.y = 3f;
            }
        }

        numActiveMolitovs = GameObject.FindGameObjectsWithTag("Grenade").Length;
        CheckForSpawn();
        
	}

    private void CheckForSpawn()
    {
        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0)
        {
            SpawnEnemy();
            enemyJustSpawned = true;
            enemySpawnCounter++;
            spawnTimer = Random.Range(spawnTimerRange.x, spawnTimerRange.y);
        }

        if (levelDistance > currentDistance) // Use this to trigger events as the level progresses
        {
            currentDistance = levelDistance;
            spawnChecker = currentDistance % 2;//spawn every 2 ground tile respawns
            if (spawnChecker == 0)
            {
                SpawnEnemy();
                enemyJustSpawned = true;
                enemySpawnCounter++;
            }

            if (Random.Range(1, 100) > 50)// 50% chance to spawn enemy on each ground respawn
            {
                SpawnEnemy();
            }

            enemyJustSpawned = false;
            enemySpawnCounter = 0;
        }
    }

    private void SpawnEnemy()
    {
        int spawnIndex = Random.Range(1, 10);  // 70/30 spawn pyro over the tire
        if (spawnIndex >= 7)
            spawnIndex = 1;
        else
            spawnIndex = 0;

        if (enemyJustSpawned)  //normal spawn is occupied, move the position just slightly
        {

            InstanciateObject(Enemies[spawnIndex], new Vector3(enemySpawnPosition.x + enemySpawnCounter, 0, 0), new Quaternion());
            enemyJustSpawned = false;

        }
        else  //pick the enemy to spawn at random from the list
        {
           
            InstanciateObject(Enemies[spawnIndex], enemySpawnPosition, new Quaternion());
        }
    }

    private void InstanciateObject(GameObject obj, Vector3 pos, Quaternion rot)
    {
        Instantiate(obj, pos, rot);
    }
}
