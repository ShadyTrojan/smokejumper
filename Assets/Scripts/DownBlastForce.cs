﻿using UnityEngine;
using System.Collections;

public class DownBlastForce : MonoBehaviour {

    private SphereCollider sphere;
    public float sphereEnd =  2.6f;
    public float expansionTime = 0.005f;
    private float sphereStart = 0.02f;
    private float multiplier;
    private float currentTime = 0;


    void Start()
    {
        sphere = GetComponent(typeof(SphereCollider)) as SphereCollider;
        sphere.radius = sphereEnd;
        multiplier = 1f / expansionTime;
        
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Hose" || other.gameObject.tag == "Ground" || other.gameObject.tag == "Water")
        {
            //do nothing
            return;
        }
        else
        {
            //other.gameObject.AddComponent<Decaytimer>().decayTime = .5f;
            other.gameObject.rigidbody.AddExplosionForce(5000, gameObject.transform.position, 1.5f);
        }
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Hose" || other.gameObject.tag == "Ground" || other.gameObject.tag == "Water")
        {
            //do nothing
            return;
        }
        else
        {
            //other.gameObject.AddComponent<Decaytimer>().decayTime = .5f;
            other.gameObject.rigidbody.AddExplosionForce(5000, gameObject.transform.position, 1.5f);
        }
    }

}
