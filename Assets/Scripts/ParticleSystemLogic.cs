﻿using UnityEngine;
using System.Collections;

public class ParticleSystemLogic : MonoBehaviour {

	// Use this for initialization
    public float lifeTimer;

	void Start () {
        //player = GameObject.Find("Player Character");
	}
	
	// Update is called once per frame
	void Update () 
    {
        lifeTimer -= Time.deltaTime;
        if(lifeTimer <= 0)
        {
            Destroy(gameObject);
        }
	}
}
