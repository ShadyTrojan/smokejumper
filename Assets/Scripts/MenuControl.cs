﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Menu Control
/// </summary>
[AddComponentMenu("2D Toolkit/Demo/tk2dUIDemoController")]
public class MenuControl : tk2dUIBaseDemoController 
{
	public bool disableOldWindows;
	public tk2dUIItem How2PlayButton;
	public tk2dUIItem CreditsButton;
	public tk2dUIItem PlayButton;
	public tk2dUIItem ExitButton;
	public tk2dUIItem TrainingButton;
	public tk2dUIItem[] ReturnToMenuButtons;
	public string firstLevel;
	public string trainingLevel;

	public GameObject MainMenuWindow;
	public GameObject CreditsWindow;

	//all how2playwindows
	public GameObject[] How2PlayWindows;
	private int currPlayWindow = 0;
	public tk2dUIItem[] NextHow2Play;
	public tk2dUIItem[] PriorHow2Play;
	
	private GameObject currWindow;

	void Awake () 
	{
		currWindow = MainMenuWindow;
		ShowWindow(MainMenuWindow.transform);
	}

	void OnEnable()
	{
		How2PlayButton.OnClick += GoToHow2Play;
		CreditsButton.OnClick += GoToCredits;
		PlayButton.OnClick += GoToGame;
		ExitButton.OnClick += ExitGame;
		TrainingButton.OnClick += Training;
		foreach (tk2dUIItem t in ReturnToMenuButtons) 
		{
			t.OnClick += GoToMainMenu;
		}
		foreach (tk2dUIItem t in PriorHow2Play) 
		{
			t.OnClick += PriorHow2PlayPage;
		}
		foreach (tk2dUIItem t in NextHow2Play) 
		{
			t.OnClick += NextHow2PlayPage;
		}
	}
	
	private void GoToMainMenu()
	{
		currPlayWindow = 0;
		MainMenuWindow.SetActive (true);
		AnimateHideWindow (currWindow.transform);
		AnimateShowWindow (MainMenuWindow.transform);
		if (disableOldWindows) 
		{
			currWindow.SetActive (false);
		}
		currWindow = MainMenuWindow;
	}

	private void GoToHow2Play()
	{
		How2PlayWindows [currPlayWindow].SetActive (true);
		AnimateHideWindow (currWindow.transform);
		AnimateShowWindow (How2PlayWindows[currPlayWindow].transform);
		if (disableOldWindows) 
		{
			currWindow.SetActive (false);
		}
		currWindow = How2PlayWindows[currPlayWindow];
	}
	private void NextHow2PlayPage()
	{
		currPlayWindow++;
		currPlayWindow %= How2PlayWindows.Length;
		GoToHow2Play ();
	}
	private void PriorHow2PlayPage()
	{
		currPlayWindow--;
		if (currPlayWindow < 0) 
		{
			currPlayWindow=0;
		}
		GoToHow2Play ();
	}
	private void GoToCredits()
	{
		if(disableOldWindows)
		{
		}
		CreditsWindow.SetActive (true);
		AnimateHideWindow (currWindow.transform);
		AnimateShowWindow (CreditsWindow.transform);
		if (disableOldWindows)
		{
			currWindow.SetActive (false);
		}
		currWindow = CreditsWindow;
	}

	private void GoToGame()
	{
		Application.LoadLevel(firstLevel);
	}
	private void Training()
	{
		Application.LoadLevel (trainingLevel);
	}
	private void ExitGame()
	{
		Application.Quit ();
	}
}
