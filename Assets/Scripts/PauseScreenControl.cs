﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Menu Control
/// </summary>
[AddComponentMenu("2D Toolkit/Demo/tk2dUIDemoController")]
public class PauseScreenControl : tk2dUIBaseDemoController 
{
	public tk2dUIItem ResumeButton;
	public tk2dUIItem MenuButton;
	public tk2dUIItem QuitButton;

	public string MenuScene;
	void OnEnable () 
	{
        Time.timeScale = 0;
        Object[] objects = FindObjectsOfType(typeof(GameObject));
        foreach (GameObject go in objects)
        {
            go.SendMessage("OnPauseGame", SendMessageOptions.DontRequireReceiver);
        }

		ResumeButton.OnClick += Resume;
		MenuButton.OnClick += Menu;
		QuitButton.OnClick += Quit;
	}

    void OnDisable()
    {
        Time.timeScale = 1;
        Object[] objects = FindObjectsOfType(typeof(GameObject));
        foreach (GameObject go in objects)
        {
            go.SendMessage("OnResumeGame", SendMessageOptions.DontRequireReceiver);
        }
    }

	void Resume()
	{
        Object[] objects = FindObjectsOfType(typeof(GameObject));
        foreach (GameObject go in objects)
        {
            go.SendMessage("OnResumeGame", SendMessageOptions.DontRequireReceiver);
        }
        Time.timeScale = 1;
		gameObject.transform.parent.gameObject.SetActive (false);
	}
	void Menu()
	{
		Application.LoadLevel (MenuScene);
	}
	void Quit()
	{
		Application.Quit ();
	}
}
